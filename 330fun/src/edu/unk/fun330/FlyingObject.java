package edu.unk.fun330;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public abstract class FlyingObject {

	protected float facing;
	protected float radius;

	protected float heading;
	protected float speed;
	protected float acceleration;

	protected float x;
	protected float y;

	public FlyingObject(float x, float y) {
		this.x = x;
		this.y = y;

		facing = 0;
		heading = 0;
		speed = 0;
		radius = 0;
	}

	// have the object react to being hit by another flying object
	// return true if the object should be subsequently removed from the universe
	public abstract boolean hitBy(FlyingObject fo, Universe u);
	
	public abstract Color getColor();
	public abstract float getRadius();
	public Image getImage() {return null;};
	
	public void setRadius(float r) {this.radius = r; }

	public abstract void paint(Graphics g);

	public float getFacing() {
		return facing;
	}

	public float getHeading() {
		return heading;
	}

	public float getSpeed() {
		return speed;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public void setFacing(float facing) {
		this.facing = facing;
	}

	protected void setHeading(float heading) {
		this.heading = heading;
	}

	protected void setSpeed(float speed) {
		this.speed = speed;
	}

	protected void setX(float x) {
		this.x = x;
	}

	protected void setY(float y) {
		this.y = y;
	}

	public float getNextX() {
		return  x + speed * (float) Math.cos(heading);
	}

	public float getNextY() { return  y + speed * (float) Math.sin(heading); }
	
	public float getNextX(int i) {
		return  x + speed * i * (float) Math.cos(heading);
	}

	public float getNextY(int i) {
		return  y + speed * i * (float) Math.sin(heading);
	}
	
	public float getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(float acceleration) {
		this.acceleration = acceleration;
	}
	
	public boolean offLeftUniverse() {
		return (x-radius < 0);
	}
	
	public boolean offRightUniverse() {
		return (x+radius> Universe.WIDTH);
	}
	
	public boolean offBottomUniverse() {
		return (y-radius<0);
	}
	
	public boolean offTopUniverse() {
		return (y+radius>Universe.HEIGHT);
	}
	
	public boolean offUniverse() {
		return (offLeftUniverse() || offRightUniverse() || offBottomUniverse() || offTopUniverse());
	}
	
	// return true if the object should be removed for leaving the universe
	public abstract boolean handleOffUniverse(Universe gd);
	/*
		if (offUniverse()){
			if(this instanceof Ship)
				Constants.logWriter.logShipDeath((Ship)this,(FlyingObject)null);
			//gd.getFlyingObjects().removeElement(this);
			return true;
		}
		return false;
	}
	*/
	
	private float distance (FlyingObject fo) {
		return Util.distance(this.getNextX(), this.getNextY(), fo.getNextX(), fo.getNextY() );
	}
	
	public boolean intersects (FlyingObject fo) {
		float distance = this.distance(fo);
		//If there is a collision
		return (distance < this.getRadius() + fo.getRadius());
	}

}
