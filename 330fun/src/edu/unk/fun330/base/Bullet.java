package edu.unk.fun330.base;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Random;

import edu.unk.fun330.Bonus;
import edu.unk.fun330.Constants;
import edu.unk.fun330.FlyingObject;
import edu.unk.fun330.Ship;
import edu.unk.fun330.Universe;
import edu.unk.fun330.base.effects.Explosion;

public class Bullet extends FlyingObject {

	public static Image bulletImage;
	
	private String creator;
	private Ship shipCreator;
	//private static final int MAX_LIFE = (int)(100*Constants.scaleX);
	protected static final int MAX_LIFE = 50; //(int)(100*Constants.scaleX);
	protected int life = 0;

	int step = 10;
	
	private int type;
	
	//private Bullet(float x, float y){super(x, y);}
	
	public Bullet(float x, float y, Ship shipCreator, int type) {
		super(x, y);
		this.creator = shipCreator.getShipController().getName();
		this.shipCreator = shipCreator;
		
		this.type = type;
		
		life = MAX_LIFE;
	}

	@Override
	public Color getColor() { return Color.GRAY; }
	
	@Override
	public float getRadius() { return 1.0f; }

	
	@Override
	public void paint(Graphics g) {
		if (life <= 0) return;
		life--;
		/*
		g.setColor(this.getColor());
		g.fillOval((int)(this.getX()-this.getRadius()) + Universe.viewPortXOffSet,
			   Constants.height - (int)(this.getY()+this.getRadius()) + Universe.viewPortYOffSet,
														(int)(this.getRadius()*2.0f),
														(int)(this.getRadius()*2.0f));
														*/
		
		if(step == -1)
			step = 10;
		
		

		int col = 10;
		int rows = 1;
		
		int width = 16;
		int height = 16;
		

		int xMod=0;
		int yMod=0;
		for(int j = 0; j < (10-step)-1; j++){
			xMod++;
			if(xMod == col){
				xMod = 0;
				yMod++;
			}
		}
		
		xMod*=width;
		yMod*=height;
		
		//yMod+=16*(shipCreator.getType()%8);
		yMod+=16*type;

		if (!Constants.SCALED_UNIVERSE)
		g.drawImage(
				
				bulletImage,
				
				0+(int)x-width/2 + Universe.viewPortXOffSet,
				Constants.height - (0+(int)y-height/2) + Universe.viewPortYOffSet,
				width+(int)x-width/2 + Universe.viewPortXOffSet,
				Constants.height - (height+(int)y-height/2) + Universe.viewPortYOffSet,
				
				0+xMod,
				height+yMod,
				width+xMod,
				0+yMod,

				null);
		else {
			int sWidth = (int)(width*Constants.scaleX);
			int sHeight = (int)(height*Constants.scaleY);
			int sX = (int)(x*Constants.scaleX);
			int sY = (int)(y*Constants.scaleY);
			g.drawImage(
					
					bulletImage,

					sX-sWidth/2,
					Constants.height - (0+(int)sY-sHeight/2),
					sWidth+sX-sWidth/2,
					Constants.height - (sHeight+sY-sHeight/2),
					
					0+xMod,
					height+yMod,
					width+xMod,
					0+yMod,
					
	  //		Color.BLACK,
					null);
		}
		
		this.step--;
		
	}

	//public Image getBulletImage() { return bulletImage; }

	/*
	public void setBulletImage(Image bulletImage) {
		this.bulletImage = bulletImage;
	}
	 */
	
	public boolean handleOffUniverse(Universe gd) {
		if (this.offUniverse() || life <= 0) {
//			gd.getBullets().remove(this);
			//gd.getFlyingObjects().removeElement(this);
			return true;
		}
		else return false;
	}
	
	// have the object react to being hit by another flying object
	// return true if the object should be removed after being hit
	public boolean hitBy (FlyingObject fo, Universe u) {
		if (fo instanceof Bonus) return false;  // bullets should pass through bonuses
		// completely remove a bullet which hits something
		//u.getBullets().removeElement(this);
		//u.getFlyingObjects().removeElement(this);
		return true;
	}
	
	public String getCreator(){
		return this.creator;
	}
	
	public Ship getShipCreator(){
		return this.shipCreator;
	}
	
}
