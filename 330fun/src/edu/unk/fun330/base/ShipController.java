package edu.unk.fun330.base;

import java.awt.Graphics;

import edu.unk.fun330.Ship;
import edu.unk.fun330.Universe;

public abstract class ShipController {
	
	protected Ship ship;
	
	public ShipController(Ship ship){
		this.ship = ship;
		ship.setShipController(this);
	}

	public abstract String getName();
	public abstract ControllerAction makeMove(Universe gd);

	public Ship getShip() {
		return ship;
	}
	
	public void paint(Graphics g){}

}
