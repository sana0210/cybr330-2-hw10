package edu.unk.fun330.base;

import edu.unk.fun330.Ship;
import edu.unk.fun330.Universe;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import edu.unk.fun330.Bonus;
import edu.unk.fun330.Constants;
import edu.unk.fun330.FlyingObject;

public class EMP extends FlyingObject {

	private Ship shipCreator;
	protected static final int MAX_LIFE = 4;
	protected int life = 0;
	private static final int RADIUS_STEP=50;
	
	public EMP(float x, float y, Ship shipCreator) {
		super(x, y);
		//this.creator = shipCreator.getShipController().getName();
		this.shipCreator = shipCreator;
		life = MAX_LIFE;
		radius = 20;
	}
	
	public Ship getShipCreator(){
		return this.shipCreator;
	}
	
	// have the object react to being hit by another flying object
	// return true if the object should be removed after being hit
	public boolean hitBy (FlyingObject fo, Universe u) {
		return false;
		
		//if (fo instanceof PoisonBonus) return true;  // EMP should destroy bombs
		
		//if (true) return false;
		// completely remove a bullet which hits something
		//u.getBullets().removeElement(this);
		//u.getFlyingObjects().removeElement(this);
		//return true;
	}
	
	public Color getColor() { return Color.GREEN; }
	public float getRadius() { return radius; }
	
	public boolean handleOffUniverse(Universe gd) {
		if (this.offUniverse() || life <= 0) {
			return true;
		}
		else return false;
	}
	
	public void paint(Graphics g) {
		if (life <= 0) return;
		life--;
		radius+=RADIUS_STEP;
		g.setColor(getColor());
		if (!Constants.SCALED_UNIVERSE) {
			g.drawOval(
					(int)(x-radius) + Universe.viewPortXOffSet,
					Constants.height - (int)(y+radius) + Universe.viewPortYOffSet,
					(int)(radius*2) ,
					(int)(radius*2));
			//g.drawString("hello",(int)x,Constants.height -(int)y+ Universe.viewPortYOffSet);
		}
		else{
			int sX = (int)(x*Constants.scaleX);
			int sY = (int)(y*Constants.scaleY);
			int sWidth = (int)(radius*2*Constants.scaleX);
			int sHeight = (int)(radius*2*Constants.scaleY);
			g.drawOval(
					(int)(sX-sWidth/2),
					Constants.height - (int)(sY+sHeight/2),
					sWidth ,
					sHeight);
			//Font f = g.getFont();  // save original font
			//g.setFont(new Font("TimesRoman", Font.PLAIN, (int)(20*Constants.scaleX)));
			//g.drawString(Integer.toString(amount),sX-sWidth/2 + 2,Constants.height -sY + sHeight/2);
			//g.setFont(f);  // restore original font
		}
	}
}
