package edu.unk.fun330;

import java.awt.Color;
import java.awt.Image;

public class EmpBonus extends Bonus {

public static Image empBonusImage;
	
	//int points;
	
	public EmpBonus(float x, float y, int amount) {
		super(x, y, amount);
	}

	//public Color getColor() { return Constants.bulletsColor; }
	public Image getImage() {return empBonusImage;};
}
