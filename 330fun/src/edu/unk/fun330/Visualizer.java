package edu.unk.fun330;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Random;
import java.util.Vector;

import javax.swing.JPanel;

import edu.unk.fun330.base.Bullet;
import edu.unk.fun330.base.effects.Cloak;
import edu.unk.fun330.base.effects.Exhaust;
import edu.unk.fun330.base.effects.Explosion;
import edu.unk.fun330.base.effects.Warp;

public class Visualizer extends JPanel {

	//private static final Color BACKGROUND = Color.WHITE;
	private static final Color BACKGROUND = Color.BLACK;
	
	private static final long serialVersionUID = 6793798862082812020L;

	private Universe gd;
	private Graphics bufferGraphics;
	private Image offscreen;
	private Dimension dim;
	private int curX, curY;
	private Image [] backImgs;
	private Image backImage1, backImage2, backImage3, backImage4, curBackImage;
	private int dx1, dx2, dy1, dy2, srcx1, srcx2, srcy1, srcy2, dx3, dy3, dx4, dy4;
	private int IMAGE_WIDTH=0;
	private int IMAGE_HEIGHT=0;
	private static final int INCREMENT=1;
	private int leadImgIdx, rearImgIdx;
	
	public Visualizer () {
		super();
		backImgs = new Image[1];  // played with switching backgrounds
		backImgs[0] = Toolkit.getDefaultToolkit().getImage("images/back0.jpg");
		//backImgs[1] = Toolkit.getDefaultToolkit().getImage("images/back1.jpg");
		//backImgs[2] = Toolkit.getDefaultToolkit().getImage("images/back2.jpg");
		//backImgs[3] = Toolkit.getDefaultToolkit().getImage("images/back3.jpg");
		leadImgIdx=0;
		rearImgIdx=0;
	}
	
	private void moveBackground() {
        if (dx1 > dim.width) {
            dx1 = 0 - dim.width;
            dx2 = 0;
            leadImgIdx = (leadImgIdx+1)%1;
        } else {
            dx1 += INCREMENT;
            dx2 += INCREMENT;
        }
        if (dx3 > dim.width) {
            dx3 = 0 - dim.width;
            dx4 = 0;
            rearImgIdx = (rearImgIdx+1)%1;
        } else {
            dx3 += INCREMENT;
            dx4 += INCREMENT;
        }
    }
	
	public void paint(Graphics g) {

		if (gd == null)
			return;

		if (IMAGE_WIDTH<=0) {
			IMAGE_WIDTH = backImgs[0].getWidth(this);
			IMAGE_HEIGHT = backImgs[0].getHeight(this);

			dim = getSize();
			//System.out.println("IMAGE_WIDTH="+IMAGE_WIDTH+" IMAGE_HEIGHT="+IMAGE_HEIGHT+" dim.width="+dim.width+" dim.height="+dim.height);
			//srcx2 = IMAGE_WIDTH; ///(IMAGE_HEIGHT/dim.height)*dim.width;

			dx1 = 0;
			dy1 = 0;
			dx2 = dim.width; //IMAGE_WIDTH;
			dy2 = dim.height; // IMAGE_HEIGHT;
			srcx1 = 0;
			srcy1 = 0;
			srcx2 = IMAGE_WIDTH; ///(IMAGE_HEIGHT/dim.height)*dim.width;
			srcy2 = IMAGE_HEIGHT;
			//srcx2 = dim.width;
			//srcy2 = dim.height;
			dx3 = dx2;
			dy3 = 0;
			dx4 = dx3+dim.width;
			dy4 = dim.height;
		}
		
		if (bufferGraphics == null) {
			dim = getSize();
			offscreen = createImage(dim.width, dim.height);
			bufferGraphics = offscreen.getGraphics();
		}
		
		moveBackground();
		
		// modify to pull from scrolling source coords in the panorama background image
		// draw background image
		//bufferGraphics.drawImage(backImage1, 0, 0, dim.width, dim.height, this);
		//bufferGraphics.drawImage(backImage1, dx1, dy1, dx2, dy2, srcx1, srcy1, srcx2, srcy2, this);
		//bufferGraphics.drawImage(backImage1, dx3, dy3, dx4, dy4, srcx1, srcy1, srcx2, srcy2, this);
		bufferGraphics.drawImage(backImgs[leadImgIdx], dx1, dy1, dx2, dy2, srcx1, srcy1, srcx2, srcy2, this);
		bufferGraphics.drawImage(backImgs[rearImgIdx], dx3, dy3, dx4, dy4, srcx1, srcy1, srcx2, srcy2, this);
		
		// bufferGraphics.setColor(Color.BLACK);
		/*
		bufferGraphics.setColor(BACKGROUND); // GRAY);
		bufferGraphics.fillRect(0, 0, dim.width, dim.height);

		bufferGraphics.setColor(BACKGROUND);
		bufferGraphics.fillRect(0 + Universe.viewPortXOffSet,
				0 + Universe.viewPortYOffSet, dim.width, dim.height);
		*/
		
		/*
		 * bufferGraphics.setColor(Color.RED); bufferGraphics.drawLine( 0 +
		 * Universe.viewPortXOffSet, 0 + Universe.viewPortYOffSet,
		 * Constants.width + Universe.viewPortXOffSet, 0 +
		 * Universe.viewPortYOffSet );
		 * 
		 * bufferGraphics.drawLine( 0 + Universe.viewPortXOffSet, 0 +
		 * Universe.viewPortYOffSet, 0 + Universe.viewPortXOffSet,
		 * Constants.height + Universe.viewPortYOffSet );
		 * 
		 * 
		 * bufferGraphics.drawLine( Constants.width + Universe.viewPortXOffSet,
		 * Constants.height + Universe.viewPortYOffSet, 0 +
		 * Universe.viewPortXOffSet, Constants.height + Universe.viewPortYOffSet );
		 * 
		 * bufferGraphics.drawLine( Constants.width + Universe.viewPortXOffSet,
		 * 0 + Universe.viewPortYOffSet, Constants.width +
		 * Universe.viewPortXOffSet, Constants.height + Universe.viewPortYOffSet );
		 */
		ArrayList toBeRemoved = new ArrayList();
		
		// Paint the ship exhausts
		gd.paintExhausts(bufferGraphics);
			
		/*
		try {
			for (Exhaust exhaust : gd.getExaust()) {
				if (exhaust.getStep() < 1)
					toBeRemoved.add(exhaust);
				else
					exhaust.paint(bufferGraphics);

			}
			for (Object o : toBeRemoved) {
				gd.getExaust().remove(o);
			}
		} catch (ConcurrentModificationException e) {
			// OHHHHHHHHHH NOOOOOOOOOO
		}
		*/

		// try {
		for (FlyingObject flyingObject : gd.getFlyingObjects()) {

			flyingObject.paint(bufferGraphics);
			bufferGraphics.setColor(Color.WHITE);

			/*
			 * bufferGraphics.drawLine((int)flyingObject.getX(),
			 * (int)flyingObject.getY(), (int)(flyingObject.getX() +
			 * (Math.sin(flyingObject.getHeading()))*flyingObject.getSpeed()*100),
			 * (int)(flyingObject.getY()
			 * +(Math.cos(flyingObject.getHeading()))*flyingObject.getSpeed()*100));
			 */

			/*
			 * bufferGraphics.setColor(Color.RED);
			 * bufferGraphics.drawLine((int)flyingObject.getX(), dim.height -
			 * (int)flyingObject.getY(), (int)(flyingObject.getX() +
			 * (Math.cos(flyingObject.getHeading()))*flyingObject.getSpeed()*100),
			 * dim.height - (int)(flyingObject.getY() +
			 * (Math.sin(flyingObject.getHeading()))*flyingObject.getSpeed()*100));
			 */

		}
		// }
		// catch (Exception e) {}

		// bufferGraphics.setColor(Color.WHITE);
		bufferGraphics.setColor(Color.BLACK);

		if (Constants.headsUpDisplay) {
			
			// Sometimes throws an exception when running at high frame rate
			for (int i = 0; i < gd.getDisplayText().size(); i++) {	
				try {
					drawString(bufferGraphics,gd.getDisplayText().get(i),10, 11 * (i + 1) + 5,1);
				}
				catch (java.lang.ArrayIndexOutOfBoundsException e) {}
			}
			
		}

		gd.paintExplosions(bufferGraphics);
		
		/*
		toBeRemoved = new ArrayList();
		for (Explosion explosion : gd.getExplosion()) {
			if (explosion.getStep() < 1)
				toBeRemoved.add(explosion);
			else
				explosion.paint(bufferGraphics);

		}
		
		for (Object o : toBeRemoved) {
			gd.getExplosion().remove(o);
		}
		*/

		toBeRemoved = new ArrayList();
		for (Warp warp : gd.getWarp()) {
			if (warp.getStep() < 1)
				toBeRemoved.add(warp);
			else
				warp.paint(bufferGraphics);

		}
		for (Object o : toBeRemoved) {
			gd.getWarp().remove(o);
		}

		toBeRemoved = new ArrayList();
		for (Cloak cloak : gd.getCloak()) {
			if (cloak.getStep() < 1)
				toBeRemoved.add(cloak);
			else
				cloak.paint(bufferGraphics);

		}
		for (Object o : toBeRemoved) {
			gd.getCloak().remove(o);
		}

		if (Constants.headsUpDisplay && !Constants.SCALED_UNIVERSE) {
			//bufferGraphics.setColor(Color.BLACK);
			//bufferGraphics.setColor(Color.WHITE);
			int boxWidth = Constants.width / 10;
			int boxHeight = Constants.height / 10;
			bufferGraphics.fillRect(3, Constants.height - 3 - boxHeight, boxWidth, boxHeight);

			Vector<Ship> ships = gd.getShips();
			ships.addAll(gd.getAIShips());

			for (int i = 0; i < ships.size(); i++) {
				Ship ship = ships.get(i);

				if (ship.isAlive()) {

					ship.getShipController().paint(bufferGraphics);
					
					int x = Constants.width / 10 + 5;
					int y = Constants.height - 74;

					/*
					if((i+1)%5 == 0){
						x += (i+1)/5 * 110;
						y -= (i+1)/5 * 70;
					}
					*/
					
					if (i > 4) {
						x += 110;
						y -= 70;
					}
					if (i > 9) {
						x += 110;
						y -= 70;
					}
					if (i > 14) {
						x += 110;
						y -= 70;
					}
					if (i > 19) {
						x += 110;
						y -= 70;
					}
					if (i > 24) {
						x += 110;
						y -= 70;
					}
					if (i > 29) {
						x += 110;
						y -= 70;
					}
					if (i > 34) {
						x += 110;
						y -= 70;
					}

					int red = (int) ((1.0 - ship.getShieldHealth()
							/ (double) Ship.SHIELD_HEALTH_FOR_LIFE) * 255);
					int green = (int) ((ship.getShieldHealth() / (double) Ship.SHIELD_HEALTH_FOR_LIFE) * 255);
					int blue = 0;

					int length = (int) ((ship.getShieldHealth() / (double) Ship.SHIELD_HEALTH_FOR_LIFE) * 100);

					// System.out.println(red + " " + green + " " + blue);
					try {
						bufferGraphics.setColor(new Color(red, green, blue));
					} catch (Exception e) {
						System.out.println(red + " " + green + " " + blue);
					}
					bufferGraphics.fillRect(x + 0, y + (i * 14), length, 11);

					if (ship instanceof AIShip)
						bufferGraphics.setColor(Color.BLUE);
					else
						bufferGraphics.setColor(Color.BLACK);

					bufferGraphics.drawRect(x + 0, y + (i * 14), length, 11);
					bufferGraphics.setColor(Color.BLACK);
					bufferGraphics.drawString(ship.getShipController()
							.getName(), x + 2, y - 4 + ((i + 1) * 14));
				}
			}

			bufferGraphics.setColor(Color.BLACK);
			//bufferGraphics.setColor(Color.WHITE);
			bufferGraphics.fillRect(3, Constants.height - boxHeight
					- 5, boxWidth, boxHeight);
			bufferGraphics.setColor(Color.GRAY);
			bufferGraphics.drawRect(3, Constants.height - boxHeight
					- 5, boxWidth, boxHeight);

			float xScale = Universe.WIDTH/boxWidth;
			float yScale = Universe.HEIGHT/boxHeight;
			for (FlyingObject flyingObject : gd.getFlyingObjects()) {

				if (flyingObject instanceof Bullet)
					bufferGraphics.setColor(Color.RED);
				else if (flyingObject instanceof AIShip)
					bufferGraphics.setColor(Color.ORANGE);
				else if (flyingObject instanceof BlackHole)
					bufferGraphics.setColor(Color.CYAN);
				else
					bufferGraphics.setColor(Color.WHITE);
					//bufferGraphics.setColor(Color.BLACK);

				bufferGraphics.fillRect(3 + (int) (flyingObject.getX() / xScale),
						Constants.height - boxHeight - 5
								+ (int) (boxHeight - flyingObject.getY() / yScale), 1, 1);
				/*
				bufferGraphics.fillRect(3 + (int) (flyingObject.getX() / 10),
						Constants.height - Constants.height / 10 - 5
								+ (int) (flyingObject.getY() / 10), 1, 1);
								*/
			
			}

		}
		/*
	    img - the specified image to be drawn
	    dx1 - the x coordinate of the first corner of the destination rectangle.
	    dy1 - the y coordinate of the first corner of the destination rectangle.
	    dx2 - the x coordinate of the second corner of the destination rectangle.
	    dy2 - the y coordinate of the second corner of the destination rectangle.
	    sx1 - the x coordinate of the first corner of the source rectangle.
	    sy1 - the y coordinate of the first corner of the source rectangle.
	    sx2 - the x coordinate of the second corner of the source rectangle.
	    sy2 - the y coordinate of the second corner of the source rectangle.
	    observer - object to be notified as more of the image is scaled and converted.
	    */


		g.drawImage(offscreen, 0, 0, this);

	}

	public void update(Graphics g) {
		paint(g);
		Toolkit.getDefaultToolkit().sync();
		// getToolkit().sync();
	}

	public void updateDisplay(Universe gd) {
		this.gd = gd;
		this.repaint();
	}
	
	
	
	
	public static void drawString(Graphics g,String s,int x, int y,int type ){
		
		for(int i = 0; i < s.length(); i++){
			
			if((int)s.charAt(i) == 32)
				continue;
		
			
			int xOff = 7 * ((int)s.charAt(i) - 32);
			int yOff = 16 * type;
			
			if((int)s.charAt(i) > 79){
				xOff-= 336;
				yOff+=8;
			}
			
			
			g.drawImage(
					Constants.font,
					
					x + (i*7),
					y,
					x + 7+(i*7),
					y + 8,
					
					0+xOff,
					0+yOff,
					7+xOff,
					8+yOff,
					
					null);
			
		}
	}
	

}
