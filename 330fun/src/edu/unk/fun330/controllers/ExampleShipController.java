package edu.unk.fun330.controllers;

import java.util.Random;

import edu.unk.fun330.Ship;
import edu.unk.fun330.Universe;
import edu.unk.fun330.base.Bullet;
import edu.unk.fun330.base.ControllerAction;
import edu.unk.fun330.base.FireBullet;
import edu.unk.fun330.base.FlightAdjustment;
import edu.unk.fun330.base.ShipController;
import edu.unk.fun330.base.ToggleShield;

public class ExampleShipController extends ShipController{

	private boolean fireBullet = false;
	private boolean toggleShield = true;
	
	public ExampleShipController(Ship ship) {
		super(ship);
	}

	@Override
	public String getName() {
		return "Really Stupid";
	}

	@Override
	/*
	 * This is the method that the engine calls to get the next move of the
	 * ship. It can call other methods and then return the move to make.
	 */
	public ControllerAction makeMove(Universe u) {
		if(fireBullet){
			fireBullet = false;
			return new FireBullet();
		}
		else
			fireBullet = true;
		
		if (toggleShield) {
			toggleShield = false;
			return new ToggleShield();
		}
		else
			toggleShield = true;
		
		FlightAdjustment fa = new FlightAdjustment();
		Random rnd = new Random();
		
		fa.setAcceleration(.05f);
		

		fa.setFacing(6.28318531f * rnd.nextFloat());
		
		return fa;
	}


}
