package edu.unk.fun330.controllers;

import java.util.Random;

import edu.unk.fun330.FlyingObject;
import edu.unk.fun330.Ship;
import edu.unk.fun330.Universe;
import edu.unk.fun330.base.Bullet;
import edu.unk.fun330.base.ControllerAction;
import edu.unk.fun330.base.FireBullet;
import edu.unk.fun330.base.FlightAdjustment;
import edu.unk.fun330.base.ShipController;

public class ExampleShipController2 extends ShipController{

	public ExampleShipController2(Ship ship) {
		super(ship);
	}

	@Override
	public String getName() {
		return "Sort of Smart";
	}


	private boolean fireBullet = false;
	
	@Override
	/*
	 * This is the method that the engine calls to get the next move of the
	 * ship. It can call other methods and then return the move to make.
	 */
	public ControllerAction makeMove(Universe u) {
		
		if(fireBullet){
			fireBullet = false;
			return new FireBullet();
		}
		else
			fireBullet = true;
			
		
		FlightAdjustment fa = new FlightAdjustment();

		FlyingObject closestObject = ship;
		float clostestObjectsDistance = Float.MAX_VALUE;
		
		for (FlyingObject flyingObject : u.getFlyingObjects()) {
			
			if(ship.equals(flyingObject))
				continue;
			
			float distance = (float)Math.sqrt(
					Math.pow(ship.getX() - flyingObject.getX(),2.0) + 
					Math.pow(ship.getY() - flyingObject.getY(),2.0));
			
			if(distance < clostestObjectsDistance){
				clostestObjectsDistance = distance;
				closestObject = flyingObject;
			}
			
		}
		
		if(ship.getX() > closestObject.getX()){
			fa.setFacing((float)Math.PI * .5f);
		}
		else{
			fa.setFacing((float)Math.PI * -.5f);
		}
		
		fa.setAcceleration(.05f);
		
		return fa;
	}

}
