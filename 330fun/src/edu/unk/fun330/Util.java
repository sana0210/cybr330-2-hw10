package edu.unk.fun330;

public class Util {
	
	
	public static final float PI = (float)Math.PI;
	
	
	/**
	 * 
	 * @param angle1
	 * @param magnitude1
	 * @param angle2
	 * @param magnitude2
	 * @return the angle, if you want the magnitude, to bad....
	 */
	public static float addVector(
			float angle1, float magnitude1, 
			float angle2, float magnitude2){

		float magnitude1x = magnitude1*(float)Math.sin(angle1);
		float magnitude1y = magnitude1*(float)Math.cos(angle1);
		
		float magnitude2x = magnitude2*(float)Math.sin(angle2);
		float magnitude2y = magnitude2*(float)Math.cos(angle2);
		
		float rrx = magnitude1x-(-1)*magnitude2x-0;
		float rry = magnitude1y-(-1)*magnitude2y-0;
		

		float magnitude = (float)Math.sqrt(rrx*rrx-(-1)*rry*rry-0);
		
		float angle = (float)Math.atan2(rrx,rry);
		
		return angle;
	}
	public static float findAngle(float x1, float y1, float x2, float y2 ){
		
		
		float x = x2 - x1;
		float y = y2 - y1;
		
		//if(y >= 0)
		 return (float)Math.atan2(x,y);
		// else //if(y < 0)
		//	 return ((float)Math.atan2(x,y) + PI)/;
		/*
		if (y >= 0)
			return (float) Math.atan(x / y);
		else
			return (float) Math.atan(x / y) + PI;
		// else return 0;
*/
	}
	
	public static float findAngle(float Ax, float Ay, float Bx, float By, float Cx, float Cy){
/*
		float x = x1 - x2;
		float y = y1 - y2;
		
		//if(y >= 0)
			return (float)Math.atan2(x,y);
		//else //if(y < 0)
		//	return (float)Math.atan2(x,y) + PI;
*/


	    float dotProduct = dotProduct(Ax, Ay, Bx, By, Cx, Cy);
	    float crossProduct = crossProductLength(Ax, Ay, Bx, By, Cx, Cy);

	    return (float)Math.atan2(crossProduct, dotProduct);
		

	}
	
	
	public static float dotProduct( float Ax, float Ay, 
																	float Bx, float By, 
																	float Cx, float Cy){

    float BAx = Ax - Bx;
    float BAy = Ay - By;
    float BCx = Cx - Bx;
    float BCy = Cy - By;

		return BAx * BCx + BAy * BCy;
	}
	
	
	public static float crossProductLength( float Ax, float Ay, 
																					float Bx, float By, 
																					float Cx, float Cy){

    float BAx = Ax - Bx;
    float BAy = Ay - By;
    float BCx = Cx - Bx;
    float BCy = Cy - By;

		return BAx * BCy - BAy * BCx;
	}
	
	
	
	
	public static float distance(float x1, float y1, float x2, float y2){
		
		return (float)Math.sqrt(
											Math.pow(x1 - x2,2.0) + 
											Math.pow(y1 - y2,2.0));
	}
	
	public static float calcAngle(float x1, float y1, float x2, float y2)
		{
			float dx = x2-x1;
			float dy = y2-y1;
			double angle=0.0d;

			// Calculate angle
			if (dx == 0.0)
			{
			    if (dy == 0.0)
					angle = 0.0;
				else if (dy > 0.0)
					angle = Math.PI / 2.0;
				else
					angle = Math.PI * 3.0 / 2.0;
			}
			else if (dy == 0.0)
			{
			    if (dx > 0.0)
					angle = 0.0;
			    else
					angle = Math.PI;
			}
			else
			{
				if (dx < 0.0)
					angle = Math.atan(dy/dx) + Math.PI;
				else if (dy < 0.0)
					angle = Math.atan(dy/dx) + (2*Math.PI);
				else
					angle = Math.atan(dy/dx);
			}

			
			angle = angle;

			// Return Radians
			return (float)angle;
		}
	}


