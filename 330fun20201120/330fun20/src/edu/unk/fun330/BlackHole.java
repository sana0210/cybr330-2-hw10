package edu.unk.fun330;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class BlackHole extends FlyingObject{

	Universe u;

	public static Image holeImage;
	public static final int RADIUS = 25;
	
	public BlackHole(float x, float y, Universe u) {
		super(x, y);
		this.u = u;
		radius = RADIUS;
	}
	
	protected boolean handleOffUniverse(Universe gd) {
		//System.out.println("off universe = " + offUniverse());
		return this.offUniverse();
	}
	
	protected boolean hitBy (FlyingObject fo, Universe u) { return false; }
	
	public Color getColor() {
		return Color.RED;
	}

	public float getRadius() { return radius; }

	//static final float MAX_DIST = Util.distance(0, 0, Constants.width, Constants.height );
	static final float MAX_DIST = Util.distance(0, 0, Universe.WIDTH, Universe.HEIGHT );
		
	private static final float ANGLE_AFFECT = RADIUS/25f; // was .1f;
	private static final float GRAVITY_AFFECT = RADIUS/5f;
	
	private void affectFlyingObjects() {
		for (FlyingObject fo : u.getFlyingObjects()) {
			//if(!(fo instanceof Bonus) && fo != this){
			if(fo != this){

				float foX = fo.getX();
				float foY = fo.getY();
				//float angle = Util.calcAngle(foX, foY, this.x, this.y);
				float dist = Util.distance(x, y, fo.getX(), fo.getY() );
				if (dist > BlackHole.RADIUS*8) continue;
				float angle = Util.calcAngle(x,y,foX, foY);
				angle = angle - ANGLE_AFFECT* (float)Math.pow(1-dist/MAX_DIST,8);
				//dist = dist - 2;
				dist = dist - GRAVITY_AFFECT*(float)Math.pow(1-dist/MAX_DIST,8); //(180*
				float nextX = x+(float)Math.cos(angle)*dist; //foX + (x - foX)/(180*(1-(MAX_DIST-dist)/MAX_DIST)); 
				float nextY = y+(float)Math.sin(angle)*dist; //foY + (y - foY)/(180*(1-(MAX_DIST-dist)/MAX_DIST));
				
				//float dist = Util.distance(this.getX(), this.getY(), fo.getX(), fo.getY() );
				//float nextX = foX + (x - foX)/(180*(1-(MAX_DIST-dist)/MAX_DIST)); //((fo.getSpeed()-(Constants.maxShipSpeed+1)*dist/MAX_DIST) * (float) Math.cos(fo.getHeading());
				//float nextY = foY + (y - foY)/(180*(1-(MAX_DIST-dist)/MAX_DIST)); //foY + (fo.getSpeed()-(Constants.maxShipSpeed+1)*dist/MAX_DIST) * (float) Math.sin(fo.getHeading());
				//float nextX = foX + ((fo.getSpeed()-(Constants.maxShipSpeed+1)*dist/MAX_DIST) * (float) Math.cos(fo.getHeading());
				//float nextY = foY + (fo.getSpeed()-(Constants.maxShipSpeed+1)*dist/MAX_DIST) * (float) Math.sin(fo.getHeading());
				
				fo.setX(nextX);
				fo.setY(nextY);
				/*
				float fox = fo.getX();
				float foy = fo.getY();
				
				float angle = 
								//Util.PI*2f - 
							  Util.findAngle(
									getX(),
									getY(),
									fo.getX(),
									fo.getY())
									//+  Util.PI/2
									;
									*/
				//fo.setHeading(angle);
								
			}
			/*
			float dist = Util.distance(this.getX(), this.getY(), fo.getX(), fo.getY() );
			fo.setX();
			fo.setY();
			*/
			
		}
	}
	
	public void paint(Graphics g) {
		affectFlyingObjects();
		
		/*
		g.setColor(Color.RED);
		if (!Constants.SCALED_UNIVERSE)
		g.drawOval(
				(int)(x-getRadius()) + Universe.viewPortXOffSet,
				Constants.height - (int)(y+this.getRadius()) + Universe.viewPortYOffSet,
				(int)(this.getRadius()*2.0f) ,
				(int)(this.getRadius()*2.0f) );
		
		else{
			int sX = (int)(x*Constants.scaleX);
			int sY = (int)(y*Constants.scaleY);
			int sRad = (int)(getRadius()*Constants.scaleX);
			g.fillOval(
					(int)(sX-sRad),
					Constants.height - (int)(sY+sRad),
					(int)sRad*2 ,
					(int)sRad*2 );
		}
*/
		
		
		int width = 300;
		int height = 308;
		if (!Constants.SCALED_UNIVERSE)
			g.drawImage(
					holeImage,
					0+(int)x-width/2 + Universe.viewPortXOffSet,
					Constants.height - (0+(int)y-height/2) + Universe.viewPortYOffSet,
					width+(int)x-width/2 + Universe.viewPortXOffSet,
					Constants.height - (height+(int)y-height/2) + Universe.viewPortYOffSet,
					0,
					height,
					width,
					0,
					null);
			else {
				int sWidth = (int)(width*Constants.scaleX);
				int sHeight = (int)(height*Constants.scaleY);
				int sX = (int)(x*Constants.scaleX);
				int sY = (int)(y*Constants.scaleY);
				g.drawImage(
						holeImage,
						sX-sWidth/2,
						Constants.height - (0+(int)sY-sHeight/2),
						sWidth+sX-sWidth/2,
						Constants.height - (sHeight+sY-sHeight/2),
						
						0,
						height,
						width,
						0,
						null);
			}
		
			
		/*
		g.drawOval((int)(x-this.getRadius()),
				Constants.height - (int)(y+this.getRadius()),
														(int)(radius*2.0f),
														(int)(radius*2.0f));
														*/
														
		/*
		g.fillOval((int)(x-4),
				Constants.height - (int)(y+4),
														(int)(4*2.0f),
														(int)(4*2.0f));
														*/
		
		
	}
	
}
