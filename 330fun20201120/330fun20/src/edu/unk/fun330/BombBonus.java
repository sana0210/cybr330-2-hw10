package edu.unk.fun330;

import java.awt.*;
import edu.unk.fun330.base.EMP;

public class BombBonus extends Bonus {

	public static Image poisonImage;
	
	public BombBonus(float x, float y, int amount) {
		super(x, y, amount);
	}

	public Color getColor() { return Constants.poisonColor; }
	public Image getImage() {return poisonImage;};
	
	// return true if the object should be removed after being hit
	protected boolean hitBy(FlyingObject fo, Universe u) {
		if (fo instanceof EMP) return true;
		else return super.hitBy(fo, u);
	}
}
