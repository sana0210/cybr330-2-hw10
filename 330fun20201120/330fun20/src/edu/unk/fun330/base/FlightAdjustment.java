package edu.unk.fun330.base;

/**
 * Flight adjustment action returned by controllers from their makeMove method
 * 
 * @author hastings
 *
 */
public class FlightAdjustment extends ControllerAction {

	private float facing;
	private float acceleration;
	private boolean stop;

	private FlightAdjustment(float facing, float acceleration, boolean stop) {
		this.facing = facing;
		this.acceleration = acceleration;
		this.stop = stop;
	}

	public FlightAdjustment(float facing, float acceleration) { this(facing, acceleration, false); }
	public FlightAdjustment() { this(0, 0, false); }
	public FlightAdjustment(boolean stop) { this(0, 0, stop); }
	public boolean getStop() { return stop; }
	public void setStop(boolean stop) { this.stop = stop; }
	public float getAcceleration() { return acceleration; }
	public void setAcceleration(float acceleration) { this.acceleration = acceleration; }
	public float getFacing() { return facing; }
	public void setFacing(float facing) { this.facing = facing; }
}
