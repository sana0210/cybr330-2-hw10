package edu.unk.fun330.base;

/**
 * Fire laser action returned by controllers from their makeMove method.
 * Works only when the ship canFireLaser method returns true.
 * 
 * @author hastings
 *
 */
public class FireLaser extends ControllerAction { }
