package edu.unk.fun330.base;

/**
 * Fire EMP action returned by controllers from their makeMove method.
 * Works only when the ship canFireEMP method returns true.
 * 
 * @author hastings
 *
 */
public class FireEMP extends ControllerAction { }
