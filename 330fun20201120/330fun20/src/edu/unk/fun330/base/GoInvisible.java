package edu.unk.fun330.base;

/**
 * Go invisible action used only by AI ships.
 * 
 * @author hastings
 *
 */
public class GoInvisible extends ControllerAction {

	public GoInvisible(){ }

}
