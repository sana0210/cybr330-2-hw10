package edu.unk.fun330.base;

/**
 * Parent class of all actions returned by ship controllers from their makeMove method.
 * 
 * @author hastings
 *
 */
public abstract class ControllerAction {

}
