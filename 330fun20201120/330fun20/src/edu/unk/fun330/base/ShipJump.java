package edu.unk.fun330.base;

/**
 * Ship jump action returned by controllers from their makeMove method.
 * Works only when the ship canTeleport method returns true.
 * 
 * @author hastings
 *
 */
public class ShipJump extends ControllerAction {
	
	private float x;
	private float y;
	
	public ShipJump(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	public float getX() { return x; }
	public float getY() { return y; }

}
