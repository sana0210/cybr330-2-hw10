package edu.unk.fun330;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

/**
 * Parent class of all game Bonuses.
 * 
 * @author hastings
 *
 */
public class Bonus extends FlyingObject {

	protected int width;
	protected int height;
	protected int amount;
	protected Image image;

	public Bonus(float x, float y, int amount) {
		super(x, y);
		width = 40;
		height = 20;
		this.amount = amount;
	}

	public int getAmount() { return amount; }
	
	@Override
	// return true if the object should be removed after being hit
	protected boolean hitBy(FlyingObject fo, Universe u) {
		if (fo instanceof Ship || fo instanceof BlackHole)
			return true;
		return false;
	}

	@Override
	protected Color getColor() { return null; }
	public float getRadius() { return 20; }
	protected boolean handleOffUniverse(Universe gd) { return false; }
	
	public void paint(Graphics g) {
		//g.drawRect(x, y, 200, 50);
		Image image = this.getImage();
		if (image == null) {
			g.setColor(getColor());
			if (!Constants.SCALED_UNIVERSE) {
				g.drawRect(
						(int)(x-width/2) + Universe.viewPortXOffSet,
						Constants.height - (int)(y+height/2) + Universe.viewPortYOffSet,
						width ,
						height);
				g.drawString("hello",(int)x,Constants.height -(int)y+ Universe.viewPortYOffSet);
			}
			else{
				int sX = (int)(x*Constants.scaleX);
				int sY = (int)(y*Constants.scaleY);
				int sWidth = (int)(width*Constants.scaleX);
				int sHeight = (int)(height*Constants.scaleY);
				g.drawRect(
						(int)(sX-sWidth/2),
						Constants.height - (int)(sY+sHeight/2),
						sWidth ,
						sHeight);
				Font f = g.getFont();  // save original font
				g.setFont(new Font("TimesRoman", Font.PLAIN, (int)(20*Constants.scaleX)));
				g.drawString(Integer.toString(amount),sX-sWidth/2 + 2,Constants.height -sY + sHeight/2);
				g.setFont(f);  // restore original font
			}
		}
		else {
			int width = 36;
			int height = 36;
			if (!Constants.SCALED_UNIVERSE)
				g.drawImage(
						image,
						0+(int)x-width/2 + Universe.viewPortXOffSet,
						Constants.height - (0+(int)y-height/2) + Universe.viewPortYOffSet,
						width+(int)x-width/2 + Universe.viewPortXOffSet,
						Constants.height - (height+(int)y-height/2) + Universe.viewPortYOffSet,
						0,
						height,
						width,
						0,
						null);
				else {
					int sWidth = (int)(width*Constants.scaleX);
					int sHeight = (int)(height*Constants.scaleY);
					int sX = (int)(x*Constants.scaleX);
					int sY = (int)(y*Constants.scaleY);
					g.drawImage(
							image,
							sX-sWidth/2,
							Constants.height - (0+(int)sY-sHeight/2),
							sWidth+sX-sWidth/2,
							Constants.height - (sHeight+sY-sHeight/2),
							
							0,
							height,
							width,
							0,
							null);
				}	
		}
	}
}
