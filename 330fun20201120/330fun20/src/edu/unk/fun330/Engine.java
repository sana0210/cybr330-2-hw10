package edu.unk.fun330;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import edu.unk.fun330.base.*;
import edu.unk.fun330.base.effects.*;
import java.lang.reflect.*;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * Game engine with main game loop.
 * 
 * @author hastings
 *
 */
public class Engine implements KeyListener {

	/** Visualizer game display. **/  
	private Visualizer v;
	
	private boolean running;
	
	/** Universe of game objects. **/  
	public Universe gd;
	
	private boolean[] keyDown;
	
	/** Collection of ship controllers. **/
	Vector<String> controllers; 
	long frames;
	Image bulletImage;
	Random rnd;

	/**
	 * Create the game engine, and connect it to the visualizer/display and ship controllers.
	 */
	public Engine(Visualizer v, Vector<String> controllers) throws Exception {
		this.v = v;
		this.controllers = controllers;

		if (this.controllers == null || this.controllers.size() == 0) {
			this.controllers = new Vector<String>();
			String[] names = { 
					"ExampleShipController",
					"ExampleShipController2",
			"HumanPlayer"};

			for (String s : names)
				this.controllers.add("edu.unk.fun330.controllers." + s);

		}
		gd = new Universe();

		keyDown = new boolean[256];
		for (int i = 0; i < keyDown.length; i++)
			keyDown[i] = false;

		gd.setKeyDown(keyDown);
		init();
	}

	/**
	 * Initialize the game engine before running it.  Includes loading the game images and connecting the controllers to ships.
	 */
	private void init() throws Exception {

		gd.reset();

		if(Constants.log) Constants.logWriter.writeHeader();

		Ship.shipImage = Toolkit.getDefaultToolkit().getImage("images/ships.png");

		Exhaust.exhaustImage = Toolkit.getDefaultToolkit().getImage("images/exhaust.png");
		Explosion.explosionImage = Toolkit.getDefaultToolkit().getImage("images/explode.png");
		Warp.warpImage = Toolkit.getDefaultToolkit().getImage("images/warp.png");
		Cloak.cloakImage = Toolkit.getDefaultToolkit().getImage("images/empburst.png");
		BlackHole.holeImage = Toolkit.getDefaultToolkit().getImage("images/blackhole2.png");
		BombBonus.poisonImage = Toolkit.getDefaultToolkit().getImage("images/bomb.png");
		PointsBonus.pointsImage = Toolkit.getDefaultToolkit().getImage("images/dollar.png");
		BulletBonus.bulletBonusImage = Toolkit.getDefaultToolkit().getImage("images/bullet.png");
		FuelBonus.fuelBonusImage = Toolkit.getDefaultToolkit().getImage("images/fuel2.png");
		ShieldBonus.shieldBonusImage = Toolkit.getDefaultToolkit().getImage("images/shield.png");
		LaserBonus.laserBonusImage = Toolkit.getDefaultToolkit().getImage("images/laser.png");
		Bullet.bulletImage = Toolkit.getDefaultToolkit().getImage("images/bombs.png");
		EmpBonus.empBonusImage = Toolkit.getDefaultToolkit().getImage("images/emp.png");
		MagnetBonus.magnetBonusImage = Toolkit.getDefaultToolkit().getImage("images/magnet2.png");
		ShipJumpBonus.shipJumpBonusImage = Toolkit.getDefaultToolkit().getImage("images/stargate.png");

		Constants.font = Toolkit.getDefaultToolkit().getImage("images/font.png");

		rnd = Constants.rnd;

		int i =0;

		for (String s: controllers) {
			try {
				//System.out.println("s = " + s);
				Class c = Class.forName(s);
				Constructor cs = c.getConstructor((new Class[] { Ship.class }));

				int x = 0;
				int y = 0;
				while(true){
					x = 40 + rnd.nextInt(Universe.WIDTH - 80);
					y = 40 + rnd.nextInt(Universe.HEIGHT - 80);	

					boolean close = false;
					for(FlyingObject fo : gd.getFlyingObjects()){
						float distance = (float)Math.sqrt(
								Math.pow(x - fo.getX(),2.0) + 
								Math.pow(y - fo.getY(),2.0));

						if(distance < 30)
							close = true;
					}


					if(!close)
						break;
				}

				Ship ship = new Ship(x, y, i);
				ship.setRadius(14.0f);

				gd.add(ship);
				ShipController sc = (ShipController) cs.newInstance((new Object[] { ship }));
				ship.setShipController(sc);
				gd.getShipControllers().add(sc);

				if(Constants.log) Constants.logWriter.logShipCreate(ship);
			} catch (Exception e) {
				e.printStackTrace();
			}
			i++;
		}

		/*
		AIShip ai = new AIShip(100,100,0);
		//ai.makeInvisible();
		gd.add(ai);
		ShipController sc = new TedAIShipController(ai);
		gd.getShipController().add(sc);
		if(Constants.log) Constants.logWriter.logShipCreate(ai);

		AIShip ai2 = new AIShip(800,600,0);
		//ai2.makeInvisible();
		gd.add(ai2);
		ShipController sc2 = new ScavengerAIShipController(ai2);
		gd.getShipController().add(sc2);
		if(Constants.log) Constants.logWriter.logShipCreate(ai2);
		 */

		//*/

		/*
		 * for(int i = 0; i < 10; i++){ ship = new
		 * Ship(175+rnd.nextInt(Constants.width),175+rnd.nextInt(Constants.height));
		 * ship.setShipImage(shipImage); gd.getShips().add(ship);
		 * gd.getShipController().add(new ExampleShipController(ship)); }
		 * 
		 * for(int i = 0; i < 10; i++){ ship = new
		 * Ship(175+rnd.nextInt(Constants.width),175+rnd.nextInt(Constants.height));
		 * ship.setShipImage(shipImage); gd.getShips().add(ship);
		 * gd.getShipController().add(new ExampleShipController2(ship)); }
		 */

		for (int j = 0; j < Constants.numberOfAsteroids; j++) {
			Asteroid a = new Asteroid(rnd.nextInt(Constants.width), rnd.nextInt(Constants.height));
			a.setRadius(Constants.asteroidRadius);
			a.setHeading(2.0f * (float) Math.PI * rnd.nextFloat());
			a.setSpeed(2.0f + 2* rnd.nextFloat());
			gd.add(a);
		}

	}

	public void stop() { running = false; }

	/**
	 * Runs the game loop of the game engine.  Spawns game objects, displays scores, checks for collisions, moves objects.
	 */
	public void run() {

		running = true;
		//boolean display = false;
		//long startTime;
		double timePassed=0;
		long lastUpdate=System.nanoTime();
		int fps = 0;
		long sleepTime=0;

		long delay = 33;

		frames = 0;
		boolean done = false;
		boolean paused = false;
		float bonusScale = this.controllers.size();
		
		while (running) {

			//if (rnd.nextFloat()*20000 < 1) {
			
			if (rnd.nextFloat()*3000 < 1) {
				int type = rnd.nextInt(4);
				int x=0, y=0;
				float heading=0;
				//System.out.println("type="+type);
				switch (type) {
					case 0:  // from top
						x = rnd.nextInt(Universe.WIDTH-2*BlackHole.RADIUS)+BlackHole.RADIUS;
						y = Universe.HEIGHT-BlackHole.RADIUS;
						heading = 3.0f * (float) Math.PI / 2.0f; //5.0f * (float) Math.PI / 4.0f;
						break;
					case 1:  // from right
						x = Universe.WIDTH-BlackHole.RADIUS;
						y = rnd.nextInt(Universe.HEIGHT-2*BlackHole.RADIUS)+BlackHole.RADIUS;
						heading = (float) Math.PI;  // go west
						break;
					case 2:  // from bottom
						x = rnd.nextInt(Universe.WIDTH-2*BlackHole.RADIUS)+BlackHole.RADIUS;
						y = 0+BlackHole.RADIUS;
						heading = (float) Math.PI / 2.0f;  // go north
						break;
					case 3:  // from left
						x=0+BlackHole.RADIUS;
						y = rnd.nextInt(Universe.HEIGHT-2*BlackHole.RADIUS)+BlackHole.RADIUS;
						heading = 0;  // go east
						break;
				}
				BlackHole bh = new BlackHole(x, y, gd);
				bh.setHeading(heading);
				bh.setSpeed(1.0f);
				gd.add(bh);
			}
			 
			/*
			if (rnd.nextFloat()*1000 < 1) {
				LifeBonus b = new LifeBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 1); //(1+rnd.nextInt(2)));
				gd.add(b);
			}
			*/
			
			// Randomly spawn bonuses. Scale the frequency based on the number of players
			if (rnd.nextFloat()*6000/bonusScale < 1) {
				ShipJumpBonus b = new ShipJumpBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 5*(1+rnd.nextInt(5)));
				gd.add(b);
			}
			
			if (rnd.nextFloat()*6000/bonusScale < 1) {
				EmpBonus b = new EmpBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 5*(1+rnd.nextInt(5)));
				gd.add(b);
			}
			
			if (rnd.nextFloat()*6000/bonusScale < 1) {
				MagnetBonus b = new MagnetBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 5*(1+rnd.nextInt(5)));
				gd.add(b);
			}
			
			if (rnd.nextFloat()*3000/bonusScale < 1) {
				BulletBonus b = new BulletBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 5*(1+rnd.nextInt(5)));
				gd.add(b);
			}

			if (rnd.nextFloat()*3000/bonusScale < 1) {
				FuelBonus f = new FuelBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 100*(1+rnd.nextInt(10)));
				gd.add(f);
			}

			if (rnd.nextFloat()*3000/bonusScale < 1) {
				PointsBonus b = new PointsBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 100*(5+rnd.nextInt(6)));
				gd.add(b);
			}

			if (rnd.nextFloat()*3000/bonusScale < 1) {
				ShieldBonus b = new ShieldBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 100*(1+rnd.nextInt(10)));
				gd.add(b);
			}

			if (rnd.nextFloat()*3000/bonusScale < 1) {
				LaserBonus b = new LaserBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 1*(1+rnd.nextInt(5)));
				gd.add(b);
			}
			
			if (rnd.nextFloat()*3000/bonusScale < 1) {
				BombBonus b = new BombBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 0);
				//PoisonBonus b = new PoisonBonus(rnd.nextInt(Universe.WIDTH), rnd.nextInt(Universe.HEIGHT), 100*(5+rnd.nextInt(6)));
				gd.add(b);
			}
			
			if(gd.getKeyDown()[KeyEvent.VK_P]){
				paused = !paused;
				gd.getKeyDown()[KeyEvent.VK_P] = false;
			}

			if(gd.getKeyDown()[KeyEvent.VK_O]){
				delay++;
				gd.getKeyDown()[KeyEvent.VK_O] = false;
			}
			if(gd.getKeyDown()[KeyEvent.VK_L]){
				if (delay > 0)
					delay--;
				gd.getKeyDown()[KeyEvent.VK_L] = false;
			}
			
			if(gd.getKeyDown()[KeyEvent.VK_G]){
				System.gc();
				gd.getKeyDown()[KeyEvent.VK_G] = false;
			}

			timePassed = (System.nanoTime()-lastUpdate)/1000000.0;
			lastUpdate=System.nanoTime();
			fps = (int) (1d / (double) timePassed * 1000d);
			
			
			gd.setDisplayText(new Vector<String>());
			gd.getDisplayText().add("O:slow L:fast P:pause");
			gd.getDisplayText().add("FPS:  " + fps);
			gd.getDisplayText().add("Cycle Time:  " + ((long)timePassed+sleepTime) + "ms");

			//THREE LOOPS? COULD BE DONE WITH ONE EASILY
			int[] cont = new int[gd.getShipControllers().size()];
			int[] scores = new int[gd.getShipControllers().size()]; 

			//load stuff into sortable stylee
			for(int i = 0; i < gd.getShipControllers().size(); i++){
				ShipController sc = gd.getShipControllers().elementAt(i);
				cont[i] = i;
				scores[i] = sc.getShip().getScore();
			}

			//Bubble sort for the win!
			for(int i = 0; i < gd.getShipControllers().size(); i++){
				for(int j = 0; j < gd.getShipControllers().size(); j++){
					if(scores[i] > scores[j]){

						int tmp1 = scores[i];
						int tmp2 = cont[i];

						scores[i] = scores[j];
						cont[i] = cont[j];

						scores[j] = tmp1;
						cont[j] = tmp2;
					} } }

			//display in order!
			for(int i = 0; i < gd.getShipControllers().size(); i++){	
				ShipController sc = gd.getShipControllers().elementAt(cont[i]);
				gd.getDisplayText().add(sc.getName()+":  " + sc.getShip().getScore());
			}
			//}

			handleSleeping();  // wake up appropriate sleeping objects

			if(!paused){ 
				checkForCollisions(); }

			for (ShipController shipController : gd.getShipControllers()) {
				calculateAcceleration(shipController);
			}

			if(!paused){
				moveObjects((long)timePassed+sleepTime);
			}
			
			//if (display)
				updateDisplay();

			timePassed = (System.nanoTime()-lastUpdate)/1000000;
			fps = (int) (1d / (double) (timePassed + sleepTime) * 1000d);
			frames++;

			sleepTime = Math.max(delay - (long)timePassed, 0);

			if(false) { //Constants.log){
				if( (gd.getShips().size() ) <= 1 && !done){
					try {
						this.init();
					} catch (Exception e) {
						e.printStackTrace();
					}
					/*
					running = false;
					done = true;
					Constants.logWriter.close();
			    javax.swing.SwingUtilities.invokeLater(new Runnable() {
		        public void run() {
		            LogAnalyzer.createAndShowGUI(Constants.logWriter.logName);
		        }
			    });
			    break;
					 */
				}
			}

			try { Thread.sleep(sleepTime, 0); } 
			catch (InterruptedException e) { e.printStackTrace(); }
		}
	}

	/**
	 * Wake up any sleeping objects and add them back to the collection of relevant flying objects.
	 */
	private void handleSleeping() {
		Vector <FlyingObject> addGroup = new Vector<FlyingObject>();
		for (FlyingObject fo : gd.getSleeping()) {
			if (fo instanceof Ship ) {
				Ship sh = (Ship) fo;
				if (!sh.isSleeping()) {
					//System.out.println("waking "+sh.getShipController().getName());
					sh.setAlive(true);
					sh.setX( rnd.nextInt(Universe.WIDTH)); 
					sh.setY( rnd.nextInt(Universe.HEIGHT)); 
					addGroup.add(fo);
				}
				else
					sh.decrementSleep();
			}
		}
		for (FlyingObject fo : addGroup) {
			gd.removeSleeping(fo);
			gd.add(fo);
		}
	}

	/*
	private void shoot() {
		for (Ship ship : gd.getShips()) {
			if (ship.isAlive()) {
				Bullet bullet = ship.getShipController().fireBullet(gd);

				if (bullet != null) {
					bullet.setBulletImage(bulletImage);
					gd.getBullets().add(bullet);
				}
			}
		}
		gd.getKeyDown()[KeyEvent.VK_SPACE] = false;
	}
	 */

	/**
	 * Check for collisions between every pair of game objects.
	 */
	private void checkForCollisions() {

		//System.out.println("sleepingObjects size="+gd.sleepingObjects.size());
		Vector<FlyingObject> flyingObjects = gd.getFlyingObjects();
		Vector<FlyingObject> addObjects = new Vector<FlyingObject>();
		Vector<FlyingObject> removeObjects = new Vector<FlyingObject>();
		for (int i = 0; i < flyingObjects.size(); i++) {  // looping through all to handle off universe

			FlyingObject flyingObject1 = flyingObjects.get(i);
			boolean remove0 = flyingObject1.handleOffUniverse(gd);
			if (remove0) {
				removeObjects.add(flyingObject1);  //add to list of objects to remove at the end of the loop
				// If a ship flies off, add to sleeping to wake up later
				if (flyingObject1 instanceof Ship) gd.addSleeping(flyingObject1);
			}

			for (int j = i + 1; j < flyingObjects.size(); j++) {
				FlyingObject flyingObject2 = flyingObjects.get(j);

				//If there is a collision...
				if ( flyingObject1.intersects(flyingObject2) ) {
					// ... have the objects react according to what hit them
					boolean remove1 = flyingObject1.hitBy(flyingObject2, gd);
					boolean remove2 = flyingObject2.hitBy(flyingObject1, gd);

					if (remove1) removeObjects.add(flyingObject1);
					if (remove2) removeObjects.add(flyingObject2);

					if (flyingObject1 instanceof Asteroid)
						addObjects.addAll(((Asteroid)flyingObject1).spawn());
					if (flyingObject2 instanceof Asteroid)
						addObjects.addAll(((Asteroid)flyingObject2).spawn());
					if (remove1 && !remove0 && (flyingObject1 instanceof Ship))
						gd.addSleeping(flyingObject1);
					if (remove2 && (flyingObject2 instanceof Ship ))
						gd.addSleeping(flyingObject2);
				}
			}
		}
		gd.addAll(addObjects);
		gd.removeAll(removeObjects);
	}

	/**
	 * Update the display based on the current game objects.
	 */
	private void updateDisplay() { v.updateDisplay(gd); }

	private void calculateAcceleration(ShipController shipController) {

		ArrayList<String> names = new ArrayList<String>();
		ArrayList<Double> times = new ArrayList<Double>();

		if(shipController.getName().equals(Constants.centerShip)){
			Universe.viewPortXOffSet = Constants.width/2 - (int)shipController.getShip().getX();
			Universe.viewPortYOffSet = (int)shipController.getShip().getY() - Constants.height/2 ;
		}

		//for (ShipController shipController : gd.getShipController()) {

		ControllerAction ca = null;

		double calculationTime;
		if (shipController.getShip().isDisabled()) {
			calculationTime = 0;
		}
		if (shipController.getShip().isAlive()) {
			calculationTime = System.nanoTime();
			ca = shipController.makeMove(gd);
			calculationTime = System.nanoTime() - calculationTime;
			if(Constants.log) Constants.logWriter.logShipAction(shipController.getShip(),ca,calculationTime);
		} 
		else {
			calculationTime = 0;
		}

		if(calculationTime != 0){
			if(names.size() == 0){
				names.add(shipController.getName());
				times.add(calculationTime);
			}
			else {
				int i;
				for(i = 0; i < names.size(); i++){
					if(times.get(i).doubleValue() > calculationTime){
						names.add(i,shipController.getName());
						times.add(i,calculationTime);
						break;
					}
				}
				if(i == names.size()){
					names.add(shipController.getName());
					times.add(calculationTime);
				}

			}
		}

		Ship ship = shipController.getShip();
		if (!ship.isAlive() || ship.isDisabled()) return;
		if (ship.isMagnetic()) ship.magnetNearbyBonuses(gd);
		
		if (ca == null) ;
		//else if (ca instanceof ShipJump && ship instanceof AIShip) {
		else if (ca instanceof ShipJump) {  // modified so that any ship can jump if they grab a warp bonus
			ShipJump sj = (ShipJump)ca;
			if (ship.canTeleport()) {
				ship.goTeleport();

				gd.getWarp().add(new Warp(ship.getX(),ship.getY()));
				//gd.getWarp().add(new Warp(sj.getX(),sj.getY()));

				ship.setX(sj.getX());
				ship.setY(sj.getY());
			}
		}
		/*
		else if (ca instanceof GoInvisible && ship instanceof AIShip) {
			AIShip ai = (AIShip) ((AIShipController)shipController).getShip();
			if (ai.canGoInvisible()) {
				//GoInvisible gi = (GoInvisible)ca;
				ai.makeInvisible();
				gd.getCloak().add(new Cloak(ai.getX(),ai.getY()));
			}
		}
		*/
		else if (ca instanceof FireLaser) {
			//FireLaser fb = (FireLaser)ca;
			Ship sh = (Ship) ((ShipController)shipController).getShip();
			if (sh.canFireLaser()) {
				sh.laserFired();
				if (Constants.SOUND) Sound.soundFireWeapon.play();

				for (int i=0; i<20; i++) {
					float x = sh.getX() + (sh.getRadius()+ 5+(i*20)) * (float)Math.cos(sh.getFacing());
					float y = sh.getY() + (sh.getRadius()+ 5+(i*20)) * (float)Math.sin(sh.getFacing());

					Laser laser = new Laser(x,y,ship);
					laser.setHeading(sh.getFacing());
					laser.setSpeed(findSpeed(sh)*1);
					//laser.setBulletImage(bulletImage);
					gd.add(laser);
				}
				}
		}
		else if (ca instanceof FireEMP) {
			//FireEMP fe = (FireEMP)ca;
			Ship sh = (Ship) ((ShipController)shipController).getShip();
			if (sh.canFireEMP()) {
				sh.empFired();
				//if (Constants.SOUND) Sound.soundFireWeapon.play();

					float x = sh.getX(); // + (sh.getRadius()+ 5+(i*20)) * (float)Math.cos(sh.getFacing());
					float y = sh.getY();// + (sh.getRadius()+ 5+(i*20)) * (float)Math.sin(sh.getFacing());

					EMP emp = new EMP(x,y,ship);
					//emp.setHeading(sh.getFacing());
					//emp.setSpeed(findSpeed(sh)*1);
					//laser.setBulletImage(bulletImage);
					gd.add(emp);
				}
		}
		else if (ca instanceof FireBullet) {
			//FireBullet fb = (FireBullet)ca;
			Ship sh = (Ship) ((ShipController)shipController).getShip();
			if (sh.canFire()) {
				sh.bulletFired();
				if (Constants.SOUND) Sound.soundFireWeapon.play();
				if(sh.getWeaponLevel() == 1){

					float x = sh.getX() + (sh.getRadius()+ 5) * (float)Math.cos(sh.getFacing());
					float y = sh.getY() + (sh.getRadius()+ 5) * (float)Math.sin(sh.getFacing());

					Bullet bullet = new Bullet(x,y,ship,sh.getWeaponLevel());
					bullet.setHeading(sh.getFacing());
					bullet.setSpeed(findSpeed(sh));
					//bullet.setBulletImage(bulletImage);


					gd.add(bullet);
				}
				else if(sh.getWeaponLevel() == 2){
					float x1 = sh.getX() + (sh.getRadius()-6) * (float)Math.cos(sh.getFacing() - Util.PI/2);
					float y1 = sh.getY() + (sh.getRadius()-6) * (float)Math.sin(sh.getFacing() - Util.PI/2);

					float x2 = sh.getX() + (sh.getRadius()-6) * (float)Math.cos(sh.getFacing() + Util.PI/2);
					float y2 = sh.getY() + (sh.getRadius()-6) * (float)Math.sin(sh.getFacing() + Util.PI/2);


					x1 = x1 + (sh.getRadius()+ 5) * (float)Math.cos(sh.getFacing());
					y1 = y1 + (sh.getRadius()+ 5) * (float)Math.sin(sh.getFacing());

					x2 = x2 + (sh.getRadius()+ 5) * (float)Math.cos(sh.getFacing());
					y2 = y2 + (sh.getRadius()+ 5) * (float)Math.sin(sh.getFacing());


					Bullet bullet = new Bullet(x1,y1,ship,sh.getWeaponLevel());
					bullet.setHeading(sh.getFacing());
					bullet.setSpeed(findSpeed(sh));
					//bullet.setBulletImage(bulletImage);
					gd.add(bullet);

					bullet = new Bullet(x2,y2,ship,sh.getWeaponLevel());
					bullet.setHeading(sh.getFacing());
					bullet.setSpeed(findSpeed(sh));
					//bullet.setBulletImage(bulletImage);
					gd.add(bullet);
				}
				else if(sh.getWeaponLevel() == 3){
					float x1 = sh.getX() + (sh.getRadius()-6) * (float)Math.cos(sh.getFacing() - Util.PI/2);
					float y1 = sh.getY() + (sh.getRadius()-6) * (float)Math.sin(sh.getFacing() - Util.PI/2);

					float x2 = sh.getX() + (sh.getRadius()-6) * (float)Math.cos(sh.getFacing());
					float y2 = sh.getY() + (sh.getRadius()-6) * (float)Math.sin(sh.getFacing());

					float x3 = sh.getX() + (sh.getRadius()-6) * (float)Math.cos(sh.getFacing() + Util.PI/2);
					float y3 = sh.getY() + (sh.getRadius()-6) * (float)Math.sin(sh.getFacing() + Util.PI/2);


					x1 = x1 + (sh.getRadius()+ 5) * (float)Math.cos(sh.getFacing());
					y1 = y1 + (sh.getRadius()+ 5) * (float)Math.sin(sh.getFacing());

					x2 = x2 + (sh.getRadius()+ 5) * (float)Math.cos(sh.getFacing());
					y2 = y2 + (sh.getRadius()+ 5) * (float)Math.sin(sh.getFacing());

					x3 = x3 + (sh.getRadius()+ 5) * (float)Math.cos(sh.getFacing());
					y3 = y3 + (sh.getRadius()+ 5) * (float)Math.sin(sh.getFacing());


					Bullet bullet = new Bullet(x1,y1,ship,sh.getWeaponLevel());
					bullet.setHeading(sh.getFacing()- .1f);
					bullet.setSpeed(findSpeed(sh));
					//bullet.setBulletImage(bulletImage);
					gd.add(bullet);

					bullet = new Bullet(x2,y2,ship,sh.getWeaponLevel());
					bullet.setHeading(sh.getFacing());
					bullet.setSpeed(findSpeed(sh));
					//bullet.setBulletImage(bulletImage);
					gd.add(bullet);

					bullet = new Bullet(x3,y3,ship,sh.getWeaponLevel());
					bullet.setHeading(sh.getFacing()+ .1f);
					bullet.setSpeed(findSpeed(sh));
					//bullet.setBulletImage(bulletImage);
					gd.add(bullet);
				}
				else if(sh.getWeaponLevel() >= 4){
					float x1 = sh.getX() + (sh.getRadius()-6) * (float)Math.cos(sh.getFacing() - Util.PI/2);
					float y1 = sh.getY() + (sh.getRadius()-6) * (float)Math.sin(sh.getFacing() - Util.PI/2);

					float x2 = sh.getX() + (sh.getRadius()-6) * (float)Math.cos(sh.getFacing() - Util.PI/3);
					float y2 = sh.getY() + (sh.getRadius()-6) * (float)Math.sin(sh.getFacing() - Util.PI/3);

					float x3 = sh.getX() + (sh.getRadius()-6) * (float)Math.cos(sh.getFacing() + Util.PI/3);
					float y3 = sh.getY() + (sh.getRadius()-6) * (float)Math.sin(sh.getFacing() + Util.PI/3);

					float x4 = sh.getX() + (sh.getRadius()-6) * (float)Math.cos(sh.getFacing() + Util.PI/2);
					float y4 = sh.getY() + (sh.getRadius()-6) * (float)Math.sin(sh.getFacing() + Util.PI/2);


					x1 = x1 + (sh.getRadius()+ 5) * (float)Math.cos(sh.getFacing());
					y1 = y1 + (sh.getRadius()+ 5) * (float)Math.sin(sh.getFacing());

					x2 = x2 + (sh.getRadius()+ 5) * (float)Math.cos(sh.getFacing());
					y2 = y2 + (sh.getRadius()+ 5) * (float)Math.sin(sh.getFacing());

					x3 = x3 + (sh.getRadius()+ 5) * (float)Math.cos(sh.getFacing());
					y3 = y3 + (sh.getRadius()+ 5) * (float)Math.sin(sh.getFacing());

					x4 = x4 + (sh.getRadius()+ 5) * (float)Math.cos(sh.getFacing());
					y4 = y4 + (sh.getRadius()+ 5) * (float)Math.sin(sh.getFacing());


					Bullet bullet = new Bullet(x1,y1,ship,4);
					bullet.setHeading(sh.getFacing()- .1f);
					bullet.setSpeed(findSpeed(sh));
					//bullet.setBulletImage(bulletImage);
					gd.add(bullet);

					bullet = new Bullet(x2,y2,ship,4);
					bullet.setHeading(sh.getFacing());
					bullet.setSpeed(findSpeed(sh));
					//bullet.setBulletImage(bulletImage);
					gd.add(bullet);

					bullet = new Bullet(x3,y3,ship,4);
					bullet.setHeading(sh.getFacing());
					bullet.setSpeed(findSpeed(sh));
					//bullet.setBulletImage(bulletImage);
					gd.add(bullet);

					bullet = new Bullet(x4,y4,ship,4);
					bullet.setHeading(sh.getFacing()+ .1f);
					bullet.setSpeed(findSpeed(sh));
					//bullet.setBulletImage(bulletImage);
					gd.add(bullet);
				}

			}
		}
		else if (ca instanceof ToggleShield) {
			Ship sh = (Ship) ((ShipController)shipController).getShip();
			sh.toggleShield();
		}
		else if (ca instanceof FlightAdjustment) {
			FlightAdjustment fa = (FlightAdjustment)ca;
			ship.setFacing(fa.getFacing()%(2f*Util.PI)); // < 0 ? fa.getFacing() + (float)Math.PI*2.0f :fa.getFacing());

			float acceleration;
			if (fa.getStop()) { if (ship.getFuelAmmount() > 0) ship.setSpeed(0); }
			else {
				//ship.setFacing(fa.getFacing() < 0 ? fa.getFacing() + (float)Math.PI*2.0f :fa.getFacing());

				acceleration = fa.getAcceleration();
				acceleration = acceleration > Constants.maxShipAccel ? Constants.maxShipAccel : acceleration;

				ship.setAcceleration(acceleration);    // try to change acceleration (fuel dependent)
			}
			
			acceleration = ship.getAcceleration(); // get updated acceleration

			float dX = (float) (ship.getSpeed() * Math.sin(ship.getHeading()) + acceleration
					* Math.sin(ship.getFacing()));
			float dY = (float) (ship.getSpeed() * Math.cos(ship.getHeading()) + acceleration
					* Math.cos(ship.getFacing()));

			float speed = (float) Math.sqrt((Math.pow(dX, 2) + Math.pow(dY, 2)));

			ship.setSpeed(speed > Constants.maxShipSpeed ? Constants.maxShipSpeed : speed);

			ship.setHeading((float) (Math.atan2(dX , dY)));

			if(acceleration > 0 && frames % 5 == 0){
				float x = ship.getX() + (float)(Math.cos(ship.getFacing() + Util.PI)*10);
				float y = ship.getY() + (float)(Math.sin(ship.getFacing() + Util.PI)*10);

				//Exhaust exhaust = new Exhaust(x,y,ship.getFacing()+(float)Math.PI);
				//gd.getExaust().add(exhaust);
				gd.addExhaust( x,y);
			}
		}
		else  // ignore any invalid actions -- added 11-20-2019
			;
	}

	private float findSpeed(Ship ship){
		float pi = (float)Math.PI;

		float facing =(float)( ship.getFacing() % (2*Util.PI) ),
				heading = (float)( ship.getHeading() % (2*Util.PI) );

		if (facing < 0)
			facing = 2*pi + facing;
		if (heading < 0)
			heading = 2*pi + heading;

		float hMf = heading - facing,
				fMh = facing - heading;

		if(heading > facing){
			if (hMf < pi/4){//add speed
				return 8f + ship.getSpeed(); 
			}
			else if (hMf > 3*pi/4 && hMf < 5*pi/4){//minus speed
				return 8f - ship.getSpeed(); 
			}
			else return 8f;
		}//end if
		else{
			if (fMh > 3*pi/4 && fMh < 5*pi/4){//minus speed
				return 8f - ship.getSpeed(); 
			}
			else if (fMh < pi/4){//add speed
				return 8f + ship.getSpeed(); 
			}
			else return 8f;
		}//end else
	}//end findSpeed

	private void moveObjects(long timePassedMs) {

		//System.out.println("flying objects size = "+gd.getFlyingObjects().size());
		//System.out.println("bullets size = "+gd.bullets.size());
		for (FlyingObject flyingObject : gd.getFlyingObjects()) {

			if (flyingObject instanceof Ship) {
				if (!((Ship) flyingObject).isAlive())
					continue;
				//System.out.println("speed = "+flyingObject.getSpeed());
			}

			float x = flyingObject.getNextX();
			float y = flyingObject.getNextY();

			// Make ships and bullets wrap around if going off the universe
			if (flyingObject instanceof Ship || flyingObject instanceof Bullet) {
				x = (x<0)?Universe.WIDTH-x : x % Universe.WIDTH;
				y = (y<0)?Universe.HEIGHT-y : y% Universe.HEIGHT;
			}
				
			flyingObject.setX(x);
			flyingObject.setY(y);
		}
	}

	public void keyTyped(KeyEvent arg0) { }

	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyCode() > 0 && arg0.getKeyCode() < keyDown.length)
			keyDown[arg0.getKeyCode()] = true;
	}

	public void keyReleased(KeyEvent arg0) {
		if (arg0.getKeyCode() > 0 && arg0.getKeyCode() < keyDown.length)
			keyDown[arg0.getKeyCode()] = false;
	}

}
